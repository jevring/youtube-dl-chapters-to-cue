package net.jevring.youtube_dl_chapter_to_cue;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 * @created 2021-04-25 18:07
 */
public class Chapter {
	@JsonProperty("start_time")
	private int startTimeInSeconds;
	@JsonProperty("end_time")
	private int endTimeInSeconds;
	private String title;

	public int getStartTimeInSeconds() {
		return startTimeInSeconds;
	}

	public void setStartTimeInSeconds(int startTimeInSeconds) {
		this.startTimeInSeconds = startTimeInSeconds;
	}

	public int getEndTimeInSeconds() {
		return endTimeInSeconds;
	}

	public void setEndTimeInSeconds(int endTimeInSeconds) {
		this.endTimeInSeconds = endTimeInSeconds;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public String toString() {
		return "Chapter{" + "startTimeInSeconds=" + startTimeInSeconds + ", endTimeInSeconds=" + endTimeInSeconds + ", title='" + title + '\'' + '}';
	}
}
