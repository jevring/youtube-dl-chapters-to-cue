package net.jevring.youtube_dl_chapter_to_cue;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Converts the chapters from the --write-json-info output of youtube-dl into a <a href="https://en.wikipedia.org/wiki/Cue_sheet_(computing)">CUE file</a>.
 *
 * @author markus@jevring.net
 * @created 2021-04-25 18:03
 */
public class Main {
	private final ObjectMapper objectMapper = new ObjectMapper();

	public void readAndWrite(Path input) throws IOException {
		String name = input.toFile().getName();
		// The file name will be whatever.info.json, and we need to drop the whole ".info.json" to match the actual mp3 file
		int dotIndex = name.indexOf('.');
		String baseName = name.substring(0, dotIndex);
		Path output = input.toAbsolutePath().getParent().resolve(baseName + ".cue");


		// todo: other metadata in the comments?
		YoutubeDlInfoJson youtubeDlInfoJson = objectMapper.readValue(input.toFile(), YoutubeDlInfoJson.class);
		StringBuilder sb = new StringBuilder();
		sb.append("TITLE \"").append(youtubeDlInfoJson.getTitle()).append("\"\r\n");
		sb.append("FILE \"").append(baseName).append(".mp3\" MP3\r\n");

		List<Chapter> chapters = youtubeDlInfoJson.getChapters();
		if (chapters == null) {
			System.err.println("There are no chapters. Exiting");
			return;
		}
		for (int i = 0; i < chapters.size(); i++) {
			Chapter chapter = chapters.get(i);
			sb.append("  TRACK ").append(String.format("%02d", i + 1)).append(" AUDIO\r\n");
			sb.append("    TITLE \"").append(chapter.getTitle()).append("\"\r\n");
			sb.append("    INDEX 01 ").append(offsetAsTimestamp(chapter.getStartTimeInSeconds())).append("\r\n");
		}

		try (OutputStream out = new FileOutputStream(output.toFile())) {
			out.write(sb.toString().getBytes(StandardCharsets.UTF_8));
		}
	}

	/**
	 * Converts an offset of seconds into MM:SS:FF, where MM is the number of minutes, SS is the number of seconds, and FF is the number of frames.
	 * Since we only have second resolution, FF will always be "00"
	 */
	private String offsetAsTimestamp(long timeOffsetInSeconds) {
		return String.format("%02d:%02d:00", timeOffsetInSeconds / 60, timeOffsetInSeconds % 60);
	}

	public static void main(String[] args) throws IOException {
		if (args.length == 0) {
			System.err.println("Usage: java -jar youtube-dl-chapters-to-cue.jar path-to-file.info.json");
			return;
		}
		Main main = new Main();
		Path input = Paths.get(args[0]).toAbsolutePath();
		main.readAndWrite(input);
	}
}
