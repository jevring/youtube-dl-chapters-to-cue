package net.jevring.youtube_dl_chapter_to_cue;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * The output of --write-json-info from youtube-dl. Only contains the parts we need.
 *
 * @author markus@jevring.net
 * @created 2021-04-25 18:07
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class YoutubeDlInfoJson {
	private List<Chapter> chapters;
	private String title;

	public List<Chapter> getChapters() {
		return chapters;
	}

	public void setChapters(List<Chapter> chapters) {
		this.chapters = chapters;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public String toString() {
		return "YoutubeDlInfoJson{" + "chapters=" + chapters + ", title='" + title + '\'' + '}';
	}
}
